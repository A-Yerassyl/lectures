## Лекция на тему: Laravel Middleware

### Введение

Сегодня мы рассмотрим одну из ключевых возможностей Laravel - middleware. Middleware в Laravel играет важную роль в обработке HTTP-запросов, обеспечивая возможность выполнять различные действия на этапе обработки запросов до и после их прохождения через ваше приложение.

### Что такое Middleware?

Middleware - это слой между HTTP-запросом и его обработкой в приложении. Он позволяет перехватывать запросы и выполнять определенные действия, такие как проверка аутентификации, логирование, управление кэшированием и многое другое.

### Принцип работы Middleware

Каждый запрос, поступающий в Laravel приложение, проходит через серию middleware, прежде чем попасть в контроллер. Аналогично, каждый ответ, выходящий из контроллера, также может быть обработан middleware. Таким образом, middleware предоставляет механизм для обработки запросов и ответов на различных этапах их жизненного цикла.

### Основные задачи Middleware

1. **Аутентификация и Авторизация**: Проверка, имеет ли пользователь доступ к запрашиваемому ресурсу.
2. **Логирование и Отслеживание**: Запись информации о запросах и действиях пользователей.
3. **Обработка кэширования**: Управление кэшированием ответов.
4. **Манипуляция с запросами и ответами**: Добавление или изменение данных запроса и ответа.
5. **Проверка и Валидация**: Проверка корректности данных запроса перед его обработкой.

### Создание Middleware

Для создания нового middleware в Laravel используется команда artisan:

```bash
php artisan make:middleware CheckAge
```

Этот командой будет создан новый файл middleware в директории `app/Http/Middleware`.

Пример содержимого файла `CheckAge`:

```php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckAge
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if ($request->age <= 200) {
            return redirect('home');
        }

        return $next($request);
    }
}
```

В данном примере middleware проверяет возраст пользователя и перенаправляет его на главную страницу, если возраст меньше или равен 200 лет.

### Регистрация Middleware

После создания middleware его необходимо зарегистрировать в приложении. Это делается в файле `app/Http/Kernel.php`. Middleware может быть зарегистрировано как глобальное или назначенное на конкретные маршруты.

#### Глобальное Middleware

Глобальное middleware применяется ко всем HTTP-запросам:

```php
protected $middleware = [
    // другие middleware
    \App\Http\Middleware\CheckAge::class,
];
```

#### Группы Middleware

Middleware может быть организовано в группы и применяться к определенным маршрутам:

```php
protected $middlewareGroups = [
    'web' => [
        // другие middleware
        \App\Http\Middleware\CheckAge::class,
    ],

    'api' => [
        // другие middleware
    ],
];
```

#### Middleware маршрута

Middleware можно назначать конкретным маршрутам или контроллерам:

```php
Route::get('profile', function () {
    // Только аутентифицированные пользователи могут получить доступ
})->middleware('auth');
```

Или в контроллере:

```php
public function __construct()
{
    $this->middleware('auth');
}
```

### Middleware термин

Middleware термин завершает обработку запроса и возвращает ответ, не передавая запрос дальше по цепочке:

```php
public function handle(Request $request, Closure $next)
{
    if ($request->age <= 200) {
        return redirect('home');
    }

    return $next($request);
}
```

### Завершение запроса в Middleware

Иногда может потребоваться выполнить действия после того, как ответ был сформирован. Для этого middleware может использовать метод `terminate`:

```php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class LogAfterRequest
{
    public function handle(Request $request, Closure $next)
    {
        return $next($request);
    }

    public function terminate($request, $response)
    {
        // Логика после ответа
    }
}
```

Метод `terminate` будет вызываться после отправки ответа клиенту.

### Заключение

Middleware - мощный инструмент в Laravel, который позволяет легко управлять запросами и ответами на различных этапах их жизненного цикла. Правильное использование middleware позволяет улучшить безопасность, производительность и управляемость вашего приложения. Надеюсь, этот материал поможет вам лучше понять и использовать middleware в ваших проектах на Laravel.



## Лекция на тему: Аутентификация и Авторизация для Web и API без использования сторонних библиотек

### Введение

Аутентификация и авторизация являются основными компонентами безопасности веб-приложений. Аутентификация подтверждает личность пользователя, а авторизация определяет, какие ресурсы доступны пользователю. В этом уроке мы рассмотрим, как реализовать аутентификацию и авторизацию в Laravel без использования сторонних библиотек.

### Аутентификация

#### Web-Аутентификация

В Laravel аутентификация для веб-приложений реализуется легко с использованием встроенных механизмов.

##### Создание таблицы пользователей

Для начала создадим миграцию для таблицы пользователей:

```bash
php artisan make:migration create_users_table --create=users
```

В файле миграции определим структуру таблицы:

```php
Schema::create('users', function (Blueprint $table) {
    $table->id();
    $table->string('name');
    $table->string('email')->unique();
    $table->string('password');
    $table->timestamps();
});
```

##### Модель User

Создадим модель пользователя:

```bash
php artisan make:model User
```

Модель будет выглядеть следующим образом:

```php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasFactory;

    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    protected $hidden = [
        'password',
    ];
}
```

##### Контроллер аутентификации

Создадим контроллер для аутентификации:

```bash
php artisan make:controller AuthController
```

В контроллере добавим методы для регистрации, входа и выхода:

```php
namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:8|confirmed',
        ]);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        Auth::login($user);

        return response()->json(['message' => 'User registered successfully'], 201);
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        if (Auth::attempt($request->only('email', 'password'))) {
            return response()->json(['message' => 'User logged in successfully'], 200);
        }

        return response()->json(['message' => 'Invalid credentials'], 401);
    }

    public function logout()
    {
        Auth::logout();

        return response()->json(['message' => 'User logged out successfully'], 200);
    }
}
```

##### Маршруты

Добавим маршруты для аутентификации в `routes/web.php`:

```php
use App\Http\Controllers\AuthController;

Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);
Route::post('logout', [AuthController::class, 'logout'])->middleware('auth');
```

#### API-Аутентификация

Для API-аутентификации мы будем использовать токены, которые будем создавать вручную.

##### Создание токенов

Добавим колонку для токена в таблицу пользователей:

```php
Schema::table('users', function (Blueprint $table) {
    $table->string('api_token', 80)->after('password')
          ->unique()
          ->nullable()
          ->default(null);
});
```

##### Генерация токена

Изменим метод `login` в `AuthController` для генерации токена:

```php
use Illuminate\Support\Str;

public function login(Request $request)
{
    $request->validate([
        'email' => 'required|string|email',
        'password' => 'required|string',
    ]);

    $user = User::where('email', $request->email)->first();

    if ($user && Hash::check($request->password, $user->password)) {
        $user->api_token = Str::random(60);
        $user->save();

        return response()->json(['token' => $user->api_token], 200);
    }

    return response()->json(['message' => 'Invalid credentials'], 401);
}
```

##### Аутентификация через токен

Добавим middleware для проверки токена:

```php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthenticateWithToken
{
    public function handle(Request $request, Closure $next)
    {
        $token = $request->header('Authorization');

        if ($token && User::where('api_token', $token)->exists()) {
            return $next($request);
        }

        return response()->json(['message' => 'Unauthorized'], 401);
    }
}
```

Зарегистрируем middleware в `app/Http/Kernel.php`:

```php
protected $routeMiddleware = [
    'auth.token' => \App\Http\Middleware\AuthenticateWithToken::class,
];
```

##### Защищенные маршруты

Добавим защищенные маршруты для API:

```php
Route::middleware('auth.token')->group(function () {
    Route::get('user', function (Request $request) {
        return $request->user();
    });
});
```

### Авторизация

В Laravel фасад `Auth` является частью системы аутентификации, предоставляемой фреймворком. Фасады в Laravel предоставляют "статический" интерфейс для классов, зарегистрированных в контейнере службы, что делает их использование более удобным. Фасад `Auth` предоставляет различные методы для работы с аутентификацией пользователей.

### Основные методы фасада `Auth`

#### 1. Аутентификация

- **attempt**: Попытка аутентификации пользователя с указанными учетными данными.

  ```php
  if (Auth::attempt(['email' => $email, 'password' => $password])) {
      // Аутентификация успешна
  }
  ```

- **login**: Выполнить вход для указанного пользователя.

  ```php
  Auth::login($user);
  ```

- **loginUsingId**: Выполнить вход для пользователя по ID.

  ```php
  Auth::loginUsingId($userId);
  ```

- **logout**: Выполнить выход текущего пользователя.

  ```php
  Auth::logout();
  ```

#### 2. Проверка статуса аутентификации

- **check**: Проверка, аутентифицирован ли пользователь.

  ```php
  if (Auth::check()) {
      // Пользователь аутентифицирован
  }
  ```

- **guest**: Проверка, что пользователь не аутентифицирован (гость).

  ```php
  if (Auth::guest()) {
      // Пользователь не аутентифицирован
  }
  ```

#### 3. Получение информации о пользователе

- **user**: Получение текущего аутентифицированного пользователя.

  ```php
  $user = Auth::user();
  ```

- **id**: Получение ID текущего аутентифицированного пользователя.

  ```php
  $userId = Auth::id();
  ```

#### 4. Проверка прав доступа

- **viaRemember**: Проверка, вошел ли пользователь с опцией "Запомнить меня".

  ```php
  if (Auth::viaRemember()) {
      // Пользователь вошел с опцией "Запомнить меня"
  }
  ```

### Пример использования Auth в контроллере

Рассмотрим пример использования `Auth` в контроллере для входа пользователя:

```php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            // Аутентификация успешна
            $request->session()->regenerate();

            return redirect()->intended('dashboard');
        }

        return back()->withErrors([
            'email' => 'The provided credentials do not match our records.',
        ]);
    }

    public function logout(Request $request)
    {
        Auth::logout();

        $request->session()->invalidate();
        $request->session()->regenerateToken();

        return redirect('/');
    }
}
```

В этом примере мы используем методы фасада `Auth` для выполнения аутентификации и выхода пользователя.

### Настройка аутентификации

Laravel предоставляет возможность конфигурации аутентификации через файл `config/auth.php`. В этом файле можно настроить охрану (guards) и провайдеров (providers).

#### Пример файла конфигурации `config/auth.php`:

```php
return [

    'defaults' => [
        'guard' => 'web',
        'passwords' => 'users',
    ],

    'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],

        'api' => [
            'driver' => 'token',
            'provider' => 'users',
            'hash' => false,
        ],
    ],

    'providers' => [
        'users' => [
            'driver' => 'eloquent',
            'model' => App\Models\User::class,
        ],
    ],

    'passwords' => [
        'users' => [
            'provider' => 'users',
            'table' => 'password_resets',
            'expire' => 60,
        ],
    ],

];
```

Этот файл конфигурации позволяет настраивать различные способы аутентификации для вашего приложения.

### Заключение

Фасад `Auth` в Laravel является мощным инструментом для работы с аутентификацией пользователей. Он предоставляет удобный и интуитивно понятный интерфейс для выполнения аутентификации, проверки статуса пользователя и управления сессиями. Использование `Auth` помогает обеспечить безопасность и удобство в работе с пользователями в вашем приложении.


### Файл конфигурации `config/auth.php`

Файл `config/auth.php` в Laravel используется для настройки системы аутентификации вашего приложения. Этот файл позволяет вам определить, как пользователи будут аутентифицироваться, какие охраны будут использоваться, и какие провайдеры данных будут обслуживать запросы аутентификации. Давайте рассмотрим основные секции этого файла и их настройки.

#### Секции файла `config/auth.php`

1. **defaults**: Устанавливает параметры по умолчанию для охраны (guard) и сброса пароля (passwords).

    ```php
    'defaults' => [
        'guard' => 'web',
        'passwords' => 'users',
    ],
    ```

    - **guard**: Определяет охрану по умолчанию, которая будет использоваться для аутентификации.
    - **passwords**: Определяет конфигурацию по умолчанию для сброса пароля.

2. **guards**: Охраны (guards) определяют, как пользователи будут аутентифицироваться для каждой охраны.

    ```php
    'guards' => [
        'web' => [
            'driver' => 'session',
            'provider' => 'users',
        ],

        'api' => [
            'driver' => 'token',
            'provider' => 'users',
            'hash' => false,
        ],
    ],
    ```

    - **web**: Использует сессии и cookies для управления аутентификацией.
    - **api**: Использует токены для управления аутентификацией API-запросов. Можно использовать различные драйверы, такие как `token`, `passport`, `sanctum`.

   Каждый guard имеет два основных параметра:
    - **driver**: Указывает на механизм аутентификации (например, `session` для веб-охран или `token` для API).
    - **provider**: Указывает, какой провайдер данных будет использоваться для поиска и проверки пользователей.

3. **providers**: Провайдеры определяют, как пользователи будут извлекаться из вашей базы данных или другого хранилища.

    ```php
    'providers' => [
        'users' => [
            'driver' => 'eloquent',
            'model' => App\Models\User::class,
        ],

        // 'users' => [
        //     'driver' => 'database',
        //     'table' => 'users',
        // ],
    ],
    ```

    - **eloquent**: Использует Eloquent ORM для работы с моделью User.
    - **database**: Использует query builder для взаимодействия с таблицей пользователей напрямую.

   В каждом провайдере можно настроить:
    - **driver**: Указывает механизм работы с данными (например, `eloquent` или `database`).
    - **model**: Указывает модель, которая будет использоваться для аутентификации (только для `eloquent`).

4. **passwords**: Сброс пароля для пользователей.

    ```php
    'passwords' => [
        'users' => [
            'provider' => 'users',
            'table' => 'password_resets',
            'expire' => 60,
        ],
    ],
    ```

    - **provider**: Указывает, какой провайдер данных будет использоваться для поиска пользователей при сбросе пароля.
    - **table**: Указывает таблицу, которая будет использоваться для хранения токенов сброса пароля.
    - **expire**: Указывает время (в минутах), в течение которого токен сброса пароля будет считаться действительным.

#### Примеры использования

##### Настройка API охраны

Если ваше приложение имеет API, и вы хотите использовать токен-базированную аутентификацию, вы можете настроить это следующим образом:

```php
'guards' => [
    'api' => [
        'driver' => 'token',
        'provider' => 'users',
    ],
],
```

##### Использование Eloquent провайдера

Если вы хотите использовать Eloquent ORM для аутентификации, настройте провайдер следующим образом:

```php
'providers' => [
    'users' => [
        'driver' => 'eloquent',
        'model' => App\Models\User::class,
    ],
],
```

##### Использование Database провайдера

Если вы хотите использовать query builder для аутентификации, настройте провайдер следующим образом:

```php
'providers' => [
    'users' => [
        'driver' => 'database',
        'table' => 'users',
    ],
],
```

##### Настройка сброса пароля

Если вам нужно настроить параметры для сброса пароля:

```php
'passwords' => [
    'users' => [
        'provider' => 'users',
        'table' => 'password_resets',
        'expire' => 60,
    ],
],
```

### Заключение

Файл `config/auth.php` в Laravel предоставляет мощные и гибкие настройки для управления аутентификацией и авторизацией в вашем приложении. Понимание его структуры и возможностей поможет вам настроить систему аутентификации в соответствии с потребностями вашего проекта.

#### Web-Авторизация

В Laravel авторизация реализуется с помощью Gate и Policy.

##### Создание Policy

Создадим policy для модели Post:

```bash
php artisan make:policy PostPolicy
```

##### Пример Policy

Пример policy для модели Post:

```php
namespace App\Policies;

use App\Models\Post;
use App\Models\User;

class PostPolicy
{
    public function view(User $user, Post $post)
    {
        return $user->id === $post->user_id;
    }

    public function update(User $user, Post $post)
    {
        return $user->id === $post->user_id;
    }
}
```

##### Регистрация Policy

Зарегистрируем policy в `AuthServiceProvider`:

```php
namespace App\Providers;

use App\Models\Post;
use App\Policies\PostPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    protected $policies = [
        Post::class => PostPolicy::class,
    ];

    public function boot()
    {
        $this->registerPolicies();
    }
}
```

##### Использование Policy

Для использования policy в контроллере:

```php
public function show(Post $post)
{
    $this->authorize('view', $post);
    return view('post.show', compact('post'));
}
```

#### API-Авторизация

Для API-авторизации также можно использовать Policy и Gate.

##### Пример API-авторизации

В контроллере API:

```php
public function update(Request $request, Post $post)
{
    $this->authorize('update', $post);

    // Обновление поста
}
```

### Заключение

Аутентификация и авторизация являются важными аспектами безопасности в веб-приложениях. Laravel предоставляет мощные встроенные инструменты для реализации этих задач без необходимости использования сторонних библиотек. Правильное использование этих инструментов поможет защитить ваши данные и обеспечить безопасное взаимодействие пользователей с приложением.