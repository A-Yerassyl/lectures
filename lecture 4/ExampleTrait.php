<?php

namespace lecture_4;

trait ExampleTrait
{
    public function getPrintClass()
    {
        return get_class($this);
    }
}

trait ExampleTrait2
{
    public function getPrintClass2()
    {
        return "Example2_" . get_class($this);
    }
}