## Лекция: Интеграция Laravel и Vue.js с использованием Vite

### Введение

Laravel и Vue.js могут быть интегрированы для создания мощных веб-приложений. В последней версии Laravel используется Vite – быстрый инструмент для сборки фронтенд-активов, который заменяет Laravel Mix.

### Установка и настройка

#### Установка Laravel

Для начала установим Laravel с помощью Composer:

```bash
composer create-project --prefer-dist laravel/laravel myApp
```

После установки настройте файл `.env`, чтобы подключить приложение к базе данных.

#### Установка Vite и Vue.js в Laravel

Laravel поставляется с Vite уже настроенным для использования Vue.js. Следующие шаги помогут настроить проект для работы с Vue.js и Vite.

1. Установите зависимости npm:

    ```bash
    npm install
    ```

2. Установите Vue.js и необходимые плагины для Vite:

    ```bash
    npm install vue@next @vitejs/plugin-vue
    ```

3. Настройте `vite.config.js` для использования плагина Vue:

    ```javascript
    import { defineConfig } from 'vite';
    import vue from '@vitejs/plugin-vue';
    import laravel from 'laravel-vite-plugin';

    export default defineConfig({
      plugins: [
        laravel({
          input: ['resources/css/app.css', 'resources/js/app.js'],
          refresh: true,
        }),
        vue(),
      ],
    });
    ```

4. Обновите файл `resources/js/app.js` для регистрации компонента Vue:

    ```javascript
    import { createApp } from 'vue';
    import ExampleComponent from './components/ExampleComponent.vue';

    const app = createApp({});
    app.component('example-component', ExampleComponent);
    app.mount('#app');
    ```

5. Создайте Vue-компонент. Например, `resources/js/components/ExampleComponent.vue`:

    ```vue
    <template>
      <div>
        <h1>{{ message }}</h1>
      </div>
    </template>

    <script>
    export default {
      data() {
        return {
          message: 'Hello from Vue.js!'
        }
      }
    }
    </script>

    <style scoped>
    h1 {
      color: blue;
    }
    </style>
    ```

6. Обновите главный шаблон Laravel (`resources/views/welcome.blade.php`) для подключения JavaScript:

    ```html
    <!DOCTYPE html>
    <html>
    <head>
        <title>Laravel</title>
        @vite('resources/css/app.css')
    </head>
    <body>
        <div id="app">
            <example-component></example-component>
        </div>
        @vite('resources/js/app.js')
    </body>
    </html>
    ```

### Работа с компонентами Vue.js в Laravel

Теперь, когда у нас настроен проект, давайте создадим несколько компонентов и покажем, как они могут взаимодействовать друг с другом.

#### Создание дополнительных компонентов

Допустим, у нас есть компонент `ChildComponent.vue`, который мы хотим использовать в `ExampleComponent.vue`.

1. Создайте файл `resources/js/components/ChildComponent.vue`:

    ```vue
    <template>
      <div>
        <p>This is a child component. Message from parent: {{ parentMessage }}</p>
      </div>
    </template>

    <script>
    export default {
      props: {
        parentMessage: {
          type: String,
          required: true
        }
      }
    }
    </script>

    <style scoped>
    p {
      color: green;
    }
    </style>
    ```

2. Обновите `ExampleComponent.vue` для использования `ChildComponent`:

    ```vue
    <template>
      <div>
        <h1>{{ message }}</h1>
        <child-component :parentMessage="message"></child-component>
      </div>
    </template>

    <script>
    import ChildComponent from './ChildComponent.vue';

    export default {
      components: {
        ChildComponent
      },
      data() {
        return {
          message: 'Hello from Vue.js!'
        }
      }
    }
    </script>

    <style scoped>
    h1 {
      color: blue;
    }
    </style>
    ```

#### Обработка форм и AJAX-запросов

Vue.js позволяет легко работать с формами и выполнять AJAX-запросы к серверу Laravel. Вот пример, как это можно сделать.

1. Создайте компонент `FormComponent.vue`:

    ```vue
    <template>
      <div>
        <form @submit.prevent="submitForm">
          <input type="text" v-model="name" placeholder="Enter your name">
          <button type="submit">Submit</button>
        </form>
        <p v-if="responseMessage">{{ responseMessage }}</p>
      </div>
    </template>

    <script>
    import axios from 'axios';

    export default {
      data() {
        return {
          name: '',
          responseMessage: ''
        }
      },
      methods: {
        async submitForm() {
          try {
            const response = await axios.post('/api/submit', { name: this.name });
            this.responseMessage = response.data.message;
          } catch (error) {
            console.error(error);
          }
        }
      }
    }
    </script>

    <style scoped>
    form {
      margin: 20px 0;
    }
    input {
      margin-right: 10px;
    }
    </style>
    ```

2. Обновите `app.js` для регистрации нового компонента:

    ```javascript
    import { createApp } from 'vue';
    import ExampleComponent from './components/ExampleComponent.vue';
    import FormComponent from './components/FormComponent.vue';

    const app = createApp({});
    app.component('example-component', ExampleComponent);
    app.component('form-component', FormComponent);
    app.mount('#app');
    ```

3. Обновите `welcome.blade.php` для включения `FormComponent`:

    ```html
    <!DOCTYPE html>
    <html>
    <head>
        <title>Laravel</title>
        @vite('resources/css/app.css')
    </head>
    <body>
        <div id="app">
            <example-component></example-component>
            <form-component></form-component>
        </div>
        @vite('resources/js/app.js')
    </body>
    </html>
    ```

4. Настройте маршрут и контроллер для обработки AJAX-запроса. Добавьте маршрут в `routes/api.php`:

    ```php
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Route;

    Route::post('/submit', function (Request $request) {
        $name = $request->input('name');
        return response()->json(['message' => 'Hello, ' . $name . '!']);
    });
    ```

### Заключение

Теперь у вас есть базовые знания для интеграции Laravel и Vue.js с использованием Vite. Вы можете создавать сложные и интерактивные интерфейсы с помощью Vue.js, обрабатывая бизнес-логику и данные на стороне сервера с помощью Laravel.

## Лекция: Интеграция Vue Router в проект на Laravel

### Введение

Vue Router является официальным маршрутизатором для Vue.js. Он позволяет создавать одностраничные приложения с маршрутизацией на клиентской стороне. В этой лекции мы рассмотрим, как интегрировать Vue Router в проект на Laravel.

### Установка и настройка Vue Router

#### Установка Vue Router

1. Установите Vue Router через npm:

    ```bash
    npm install vue-router@next
    ```

2. Обновите `resources/js/app.js`, чтобы включить Vue Router:

    ```javascript
    import { createApp } from 'vue';
    import { createRouter, createWebHistory } from 'vue-router';
    import App from './App.vue';
    import routes from './routes';

    const router = createRouter({
      history: createWebHistory(),
      routes,
    });

    const app = createApp(App);
    app.use(router);
    app.mount('#app');
    ```

3. Создайте файл `resources/js/routes.js` для определения маршрутов:

    ```javascript
    import ExampleComponent from './components/ExampleComponent.vue';
    import AnotherComponent from './components/AnotherComponent.vue';

    const routes = [
      { path: '/', component: ExampleComponent },
      { path: '/another', component: AnotherComponent },
    ];

    export default routes;
    ```

#### Создание Vue-компонентов

Создайте несколько Vue-компонентов, которые будут использоваться в маршрутах.

1. `ExampleComponent.vue`:

    ```vue
    <template>
      <div>
        <h1>Home</h1>
        <p>{{ message }}</p>
      </div>
    </template>

    <script>
    export default {
      data() {
        return {
          message: 'Welcome to the Home Page!'
        }
      }
    }
    </script>

    <style scoped>
    h1 {
      color: blue;
    }
    </style>
    ```

2. `AnotherComponent.vue`:

    ```vue
    <template>
      <div>
        <h1>Another Page</h1>
        <p>This is another page.</p>
      </div>
    </template>

    <script>
    export default {
      data() {
        return {
          message: 'This is another page!'
        }
      }
    }
    </script>

    <style scoped>
    h1 {
      color: green;
    }
    </style>
    ```

#### Создание основного компонента приложения

Создайте основной компонент приложения `App.vue`:

```vue
<template>
  <div id="app">
    <nav>
      <router-link to="/">Home</router-link>
      <router-link to="/another">Another</router-link>
    </nav>
    <router-view></router-view>
  </div>
</template>

<script>
export default {
  name: 'App',
};
</script>

<style>
nav {
  display: flex;
  gap: 10px;
}

router-link {
  text-decoration: none;
  color: blue;
}

router-link.active {
  font-weight: bold;
  color: red;
}
</style>
```

#### Обновление шаблона Laravel

Обновите главный шаблон Laravel (`resources/views/welcome.blade.php`) для подключения нового компонента:

```html
<!DOCTYPE html>
<html>
<head>
    <title>Laravel</title>
    @vite('resources/css/app.css')
</head>
<body>
    <div id="app"></div>
    @vite('resources/js/app.js')
</body>
</html>
```

### Пример использования маршрутизации

Теперь ваше приложение будет использовать Vue Router для управления маршрутизацией на клиентской стороне. Перейдите к маршруту `/another` и увидите другой компонент.

#### Создание новых маршрутов и компонентов

Добавьте дополнительные маршруты и компоненты в `routes.js` и создайте соответствующие компоненты. Например, добавим маршрут для страницы "Contact":

1. Обновите `routes.js`:

    ```javascript
    import ExampleComponent from './components/ExampleComponent.vue';
    import AnotherComponent from './components/AnotherComponent.vue';
    import ContactComponent from './components/ContactComponent.vue';

    const routes = [
      { path: '/', component: ExampleComponent },
      { path: '/another', component: AnotherComponent },
      { path: '/contact', component: ContactComponent },
    ];

    export default routes;
    ```

2. Создайте файл `resources/js/components/ContactComponent.vue`:

    ```vue
    <template>
      <div>
        <h1>Contact</h1>
        <p>Contact us at contact@example.com</p>
      </div>
    </template>

    <script>
    export default {
      data() {
        return {
          message: 'This is the contact page!'
        }
      }
    }
    </script>

    <style scoped>
    h1 {
      color: purple;
    }
    </style>
    ```

### Заключение

Теперь у вас есть базовые знания для интеграции Vue Router в проект на Laravel. Это позволяет создавать одностраничные приложения с маршрутизацией на клиентской стороне, улучшая пользовательский опыт и организуя структуру приложения. Вы можете добавлять новые маршруты и компоненты по мере необходимости, делая ваше приложение более динамичным и интерактивным.
