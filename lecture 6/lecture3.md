## Часть 2: Основы тестирования в Laravel

### Введение в тестирование

Тестирование - это важная часть разработки, которая позволяет убедиться, что ваш код работает правильно. Laravel включает в себя PHPUnit и имеет встроенные средства для тестирования приложений.

### Настройка тестов

Тесты в Laravel хранятся в папке `tests`. Laravel по умолчанию создает примеры тестов при установке.

### Создание теста

Создадим тест для API статей:

```sh
php artisan make:test ArticleApiTest
```

Файл: `tests/Feature/ArticleApiTest.php`

```php
namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\Article;

class ArticleApiTest extends TestCase
{
    use RefreshDatabase;

    public function test_can_create_article()
    {
        $response = $this->postJson('/api/articles', [
            'title' => 'Test Article',
            'content' => 'This is a test article.',
        ]);

        $response->assertStatus(201);
        $this->assertDatabaseHas('articles', ['title' => 'Test Article']);
    }

    public function test_can_get_articles()
    {
        $articles = Article::factory()->count(3)->create();

        $response = $this->getJson('/api/articles');

        $response->assertStatus(200)
                 ->assertJsonCount(3);
    }

    public function test_can_update_article()
    {
        $article = Article::factory()->create();

        $response = $this->putJson('/api/articles/' . $article->id, [
            'title' => 'Updated Article',
            'content' => 'This is an updated test article.',
        ]);

        $response->assertStatus(200);
        $this->assertDatabaseHas('articles', ['title' => 'Updated Article']);
    }

   

 public function test_can_delete_article()
    {
        $article = Article::factory()->create();

        $response = $this->deleteJson('/api/articles/' . $article->id);

        $response->assertStatus(200);
        $this->assertDatabaseMissing('articles', ['id' => $article->id]);
    }
}
```

### Запуск тестов

Запустим тесты с помощью команды:

```sh
php artisan test
```

## Заключение

Сегодня мы узнали, как создавать RESTful API с использованием Laravel и как тестировать наше приложение. Эти навыки помогут вам создавать надежные и масштабируемые веб-приложения. Не забывайте тестировать ваш код, чтобы убедиться, что он работает корректно.

Спасибо за внимание! Теперь у нас есть время для вопросов и ответов.