
# Лекция: Работа с базой данных в Laravel

Сегодняшняя лекция будет посвящена работе с базой данных в Laravel. Мы рассмотрим создание и управление таблицами с использованием миграций, взаимодействие с базой данных через Eloquent ORM, а также определения и использование отношений между моделями и выполнение сложных запросов.

## Часть 1: Создание и управление таблицами с использованием миграций

### Введение в миграции

Миграции - это способ управления схемой базы данных с помощью кода. Они позволяют легко создавать и изменять таблицы и их структуры.

### Создание миграций

Чтобы создать миграцию, используйте Artisan-команду:

```sh
php artisan make:migration create_users_table
```

Этот файл будет создан в папке `database/migrations`.

### Структура миграций

Файл миграции содержит два метода: `up` и `down`. Метод `up` используется для определения изменений, которые должны быть внесены в базу данных, а метод `down` для их отмены.

Пример миграции:

```php
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }
}
```

### Выполнение миграций

Чтобы применить миграции и внести изменения в базу данных, используйте команду:

```sh
php artisan migrate
```

Чтобы откатить последнюю миграцию, используйте команду:

```sh
php artisan migrate:rollback
```

## Часть 2: Работа с Eloquent ORM для взаимодействия с базой данных

### Введение в Eloquent ORM

Eloquent ORM - это объектно-реляционное отображение (ORM) в Laravel. Оно позволяет работать с базой данных, используя модели, которые представляют собой таблицы базы данных.

### Создание модели

Чтобы создать модель, используйте Artisan-команду:

```sh
php artisan make:model User
```

Эта команда создаст файл `User.php` в папке `app/Models`.

### Определение модели

Пример модели:

```php
namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', 'email', 'password',
    ];
}
```

### Работа с базой данных через модели

#### Создание записей

Чтобы создать новую запись в таблице `users`, используйте метод `create`:

```php
$user = User::create([
    'name' => 'John Doe',
    'email' => 'john@example.com',
    'password' => bcrypt('password'),
]);
```

#### Чтение записей

Чтобы получить все записи из таблицы `users`, используйте метод `all`:

```php
$users = User::all();
```

Чтобы найти конкретную запись по ID, используйте метод `find`:

```php
$user = User::find(1);
```

#### Обновление записей

Чтобы обновить существующую запись, найдите её и используйте метод `update`:

```php
$user = User::find(1);
$user->update([
    'name' => 'Jane Doe',
]);
```

#### Удаление записей

Чтобы удалить запись, найдите её и используйте метод `delete`:

```php
$user = User::find(1);
$user->delete();
```

## Часть 3: Определение и использование отношений между моделями

### Введение в отношения Eloquent

Eloquent поддерживает различные типы отношений между моделями: один-к-одному, один-ко-многим, многие-ко-многим, полиморфные отношения и т.д.

### Один-к-одному (One-to-One)

Пример отношения один-к-одному:

```php
class User extends Model
{
    public function profile()
    {
        return $this->hasOne(Profile::class);
    }
}

class Profile extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
```

### Один-ко-многим (One-to-Many)

Пример отношения один-ко-многим:

```php
class Post extends Model
{
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}

class Comment extends Model
{
    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
```

### Многие-ко-многим (Many-to-Many)

Пример отношения многие-ко-многим:

```php
class User extends Model
{
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }
}

class Role extends Model
{
    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
```

## Часть 4: Выполнение сложных запросов с Eloquent

### Фильтрация данных

Вы можете использовать методы `where` для фильтрации данных:

```php
$users = User::where('email', 'like', '%@example.com%')->get();
```

### Сортировка данных

Для сортировки данных используйте метод `orderBy`:

```php
$users = User::orderBy('name', 'asc')->get();
```

### Пагинация

Для пагинации данных используйте метод `paginate`:

```php
$users = User::paginate(10);
```

### Объединение (Join) таблиц

Вы можете использовать метод `join` для объединения таблиц:

```php
$users = DB::table('users')
            ->join('posts', 'users.id', '=', 'posts.user_id')
            ->select('users.*', 'posts.title')
            ->get();
```

### Агрегатные функции

Eloquent поддерживает различные агрегатные функции, такие как `count`, `max`, `min`, `avg`, `sum`:

```php
$userCount = User::count();
$maxAge = User::max('age');
```

### Использование отношений в запросах

Вы можете использовать методы отношений в запросах для загрузки связанных данных:

```php
$users = User::with('profile')->get();
```

## Заключение

В этой лекции мы рассмотрели основы работы с базой данных в Laravel, включая создание и управление таблицами с помощью миграций, использование Eloquent ORM для взаимодействия с базой данных, определение и использование отношений между моделями, а также выполнение сложных запросов. Эти знания помогут вам эффективно работать с базой данных и создавать мощные и масштабируемые веб-приложения на Laravel.

Спасибо за внимание! Теперь у нас есть время для вопросов и ответов.